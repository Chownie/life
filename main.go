package main

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

type cell struct {
	live       bool
	neighbours int
}

type cellMap struct {
	cells  [][]*cell
	width  int
	height int
}

func main() {
	err := sdl.Init(sdl.INIT_EVERYTHING)
	if err != nil {
		fmt.Println(err)
	}
	defer sdl.Quit()

	win, err := sdl.CreateWindow("Life",
		sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		898, 638, sdl.WINDOW_SHOWN)
	if err != nil {
		fmt.Println(err)
	}
	defer win.Destroy()

	windowSurface, err := win.GetSurface()
	if err != nil {
		fmt.Println(err)
	}
	defer windowSurface.Free()

	cellSurface, err := sdl.CreateRGBSurface(0, 768, 512, 32, 0, 0, 0, 0)
	if err != nil {
		fmt.Println(err)
	}
	defer cellSurface.Free()

	offset := int32(126)
	playButton := sdl.Rect{1, 1, 126, 126}
	windowSurface.FillRect(&playButton, 0x0000ff)

	nextButton := sdl.Rect{128, 1, 126, 126}
	windowSurface.FillRect(&nextButton, 0x00ff00)

	cells := cellMap{}
	cells.resetCells(48, 22)

	drawTiles(cellSurface, cells)
	running := true
	playing := false
	tick := 0
	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.QuitEvent:
				running = false
			case *sdl.MouseButtonEvent:
				if t.Type == sdl.MOUSEBUTTONDOWN {
					point := sdl.Point{t.X, t.Y}
					if point.InRect(&sdl.Rect{0, offset, cellSurface.W, cellSurface.H}) {
						x := int(t.X) / 16
						y := int(t.Y-offset) / 16
						cells.toggle(x, y)
					}
					if point.InRect(&playButton) {
						playing = !playing
					}
					if point.InRect(&nextButton) {
						cells.filter(cells.countNeighbours)
						cells.filter(cells.progress)
					}

				}
			}
		}

		if playing {
			if tick%2 == 0 {
				cells.filter(cells.countNeighbours)
				cells.filter(cells.progress)
			}
		}

		drawTiles(cellSurface, cells)
		cellSurface.Blit(nil,
			windowSurface, &sdl.Rect{1, 127, 898, 1026})
		win.UpdateSurface()

		sdl.Delay(50)
		tick++
	}
}

func drawTiles(surface *sdl.Surface, tiles cellMap) {
	for x, row := range tiles.cells {
		for y, cell := range row {
			colour := uint32(0x000000)
			if !cell.live {
				colour = 0xffffff
			}
			surface.FillRect(&sdl.Rect{int32(x * 16), int32(y * 16),
				15, 15}, colour)
		}
	}
}

func clamp(in int, min int, max int) int {
	if in < min {
		return min
	}
	if in > max {
		return max
	}
	return in
}

func (cm *cellMap) toggle(x int, y int) {
	cm.cells[x][y].live = !cm.cells[x][y].live
}

func (cm *cellMap) progress(_ int, _ int, currentCell *cell) {
	if currentCell.neighbours == 3 {
		currentCell.live = true
	} else if currentCell.neighbours != 4 {
		currentCell.live = false
	}
}

func (cm *cellMap) getCell(x int, y int) *cell {
	//fmt.Println(x, y)
	return cm.cells[((x%cm.width)+cm.width)%cm.width][((y%cm.height)+cm.height)%cm.height]
}

func (cm *cellMap) countNeighbours(x int, y int, currentCell *cell) {
	top := y - 1
	left := x - 1
	bottom := y + 2
	right := x + 2

	subtotal := 0
	for i := left; i < right; i++ {
		for j := top; j < bottom; j++ {
			if cm.getCell(i, j).live {
				subtotal++
			}
		}
	}
	currentCell.neighbours = subtotal
}

func (cm *cellMap) filter(input func(int, int, *cell)) {
	for x, column := range cm.cells {
		for y, cell := range column {
			input(x, y, cell)
		}
	}
}

func (cm *cellMap) resetCells(width int, height int) {
	cm.width = width
	cm.height = height
	cm.cells = make([][]*cell, width)
	for x := 0; x < width; x++ {
		cm.cells[x] = make([]*cell, height)
		for y := 0; y < height; y++ {
			cm.cells[x][y] = &cell{false, 0}
		}
	}
}
