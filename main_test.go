package main

import (
	"testing"
)

var cm cellMap

func init() {
	cm.resetCells(10, 10)
}
func TestUnderpopulation(t *testing.T) {
	cm.resetCells(10, 10)
	cm.getCell(5, 5).live = true
	cm.getCell(5, 6).live = true
	cm.filter(cm.countNeighbours)
	cm.filter(cm.progress)

	if cm.getCell(5, 5).live {
		t.Error("Underpopulated cell didn't die")
	}
}

func TestOverpopulation(t *testing.T) {
	cm.resetCells(10, 10)
	cm.getCell(4, 5).live = true
	cm.getCell(4, 4).live = true
	cm.getCell(5, 5).live = true
	cm.getCell(4, 6).live = true
	cm.getCell(5, 6).live = true
	cm.filter(cm.countNeighbours)
	cm.filter(cm.progress)

	if cm.getCell(5, 5).live {
		t.Error("Overpopulated cell didn't die")
	}
}

func TestGeneration(t *testing.T) {
	cm.resetCells(10, 10)
	cm.getCell(4, 5).live = true
	cm.getCell(5, 4).live = true
	cm.getCell(5, 5).live = true
	cm.filter(cm.countNeighbours)
	cm.filter(cm.progress)

	if !cm.getCell(5, 5).live {
		t.Error("Healthy cell failed to live on")
	}
}

func TestReproduction(t *testing.T) {
	cm.resetCells(10, 10)
	cm.getCell(5, 5).live = false
	cm.getCell(4, 5).live = true
	cm.getCell(4, 4).live = true
	cm.getCell(4, 6).live = true
	cm.filter(cm.countNeighbours)
	cm.filter(cm.progress)

	if !cm.getCell(5, 5).live {
		t.Error("Dead cell failed to appear from reproduction")
	}
}

func TestBlinker(t *testing.T) {
	cm.resetCells(10, 10)
	cm.getCell(5, 4).live = true
	cm.getCell(5, 5).live = true
	cm.getCell(5, 6).live = true
	cm.filter(cm.countNeighbours)
	cm.filter(cm.progress)

	if !cm.getCell(4, 5).live || cm.getCell(5, 6).live {
		t.Error("Blinker failed to phase")
	}
}

func TestGlider(t *testing.T) {
	cm.resetCells(15, 15)
	cm.getCell(3, 1).live = true
	cm.getCell(4, 2).live = true
	cm.getCell(4, 3).live = true
	cm.getCell(3, 3).live = true
	cm.getCell(2, 3).live = true
	cm.filter(cm.countNeighbours)
	cm.filter(cm.progress)
	cm.filter(cm.countNeighbours)

	if !cm.getCell(2, 2).live || !cm.getCell(2, 3).live {
		t.Error("Glider failed to phase")
	}
}
